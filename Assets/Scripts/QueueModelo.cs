﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QueueModelo : MonoBehaviour
{
    public int queueSize;
    public string[] queue;

    public int currentNumber = 1;
    public int currentLetter = 0;
    public int currentPosition = 0;

    public string[] letters = { "A", "B", "C", "D", "E" };

    public int currentCartel = 1;

    public int elementoAMostrar = 0;
}
