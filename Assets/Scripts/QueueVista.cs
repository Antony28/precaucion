﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QueueVista : MonoBehaviour
{
    QueueModelo q_modelo;

    public Text[] textosVisibles;

    void Start()
    {
        q_modelo = GetComponent<QueueModelo>();
    }
    public void mostrar()
    {
        if (q_modelo.currentCartel == 1)
        {
            textosVisibles[0].text = q_modelo.queue[q_modelo.elementoAMostrar];
            q_modelo.elementoAMostrar++;
            q_modelo.currentCartel++;
        }
        else if (q_modelo.currentCartel == 2)
        {
            textosVisibles[1].text = q_modelo.queue[q_modelo.elementoAMostrar];
            q_modelo.elementoAMostrar++;
            q_modelo.currentCartel++;
        }
        else if (q_modelo.currentCartel == 3)
        {
            textosVisibles[2].text = q_modelo.queue[q_modelo.elementoAMostrar];
            q_modelo.elementoAMostrar++;
            q_modelo.currentCartel++;

        }
        else if (q_modelo.currentCartel == 4)
        {
            textosVisibles[3].text = q_modelo.queue[q_modelo.elementoAMostrar];
            q_modelo.elementoAMostrar++;
            q_modelo.currentCartel++;
        }
        else
        {
            textosVisibles[Random.Range(0, 3)].text = q_modelo.queue[q_modelo.elementoAMostrar];
            q_modelo.currentCartel++;
            q_modelo.elementoAMostrar++;
        }
    }
}
