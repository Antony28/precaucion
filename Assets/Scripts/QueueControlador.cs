﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QueueControlador : MonoBehaviour
{
    QueueModelo q_modelo;
    void Start()
    {
        q_modelo = GetComponent<QueueModelo>();

        q_modelo.queue = new string[q_modelo.queueSize];
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            addClientToQueue();
        }

        if (q_modelo.currentLetter == 5)
        {
            q_modelo.currentLetter = 0;
            q_modelo.currentNumber++;
        }
    }
    void addClientToQueue()
    {
        q_modelo.queue[q_modelo.currentPosition] = q_modelo.currentNumber + q_modelo.letters[q_modelo.currentLetter];
        Debug.Log("Cliente Agregado: " + q_modelo.currentNumber + q_modelo.letters[q_modelo.currentLetter]);
        q_modelo.currentPosition++;
        q_modelo.currentLetter++;
    }
}
